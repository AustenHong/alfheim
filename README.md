Greetings to all players and those who just passed by in the Alfheim repository!

Alfheim is an addon to the [Botania](http://botaniamod.net/) mod written for [Minecraft](https://minecraft.net/). It adds one of the worlds of the [Scandinavian mythology](https://en.wikipedia.org/wiki/Álfheimr), the prerequisites for which were in the Botania mod.

Alfheim is licensed under the [Botania License](http://botaniamod.net/license.php) 

You can also visit the official addon information channel in the [Telegram](https://t.me/AlfheimOfficial/).

If you want to get in-dev version for whatever reason you just need to download "**Development**" branch (from *Downloads* in the left sidebar), extract archive wherever you want and launch **setup.bat**, entering **4** when asked (or just enter *gradlew build* in command line). .jar file will be placed in */build/libs/*
Don't forget to install latest versions of JDK**8**. 