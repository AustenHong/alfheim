package alfheim.api.lib

import cpw.mods.fml.client.registry.RenderingRegistry

object LibRenderIDs {
	
	// Alfheim
	val idAniTorch = RenderingRegistry.getNextAvailableRenderId()
	val idAnyavil = RenderingRegistry.getNextAvailableRenderId()
	val idHarvester = RenderingRegistry.getNextAvailableRenderId()
	val idManaAccelerator = RenderingRegistry.getNextAvailableRenderId()
	val idPowerStone = RenderingRegistry.getNextAvailableRenderId()
	val idPylon = RenderingRegistry.getNextAvailableRenderId()
	val idShrinePanel = RenderingRegistry.getNextAvailableRenderId()
	val idTransferer = RenderingRegistry.getNextAvailableRenderId()
	
	// Iridescence
	val idDoubleFlower = RenderingRegistry.getNextAvailableRenderId()
	val idMultipass = RenderingRegistry.getNextAvailableRenderId()
	val idHopper = RenderingRegistry.getNextAvailableRenderId()
}