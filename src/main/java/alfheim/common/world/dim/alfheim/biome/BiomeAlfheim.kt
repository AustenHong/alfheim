package alfheim.common.world.dim.alfheim.biome

import alfheim.AlfheimCore
import alfheim.common.block.AlfheimBlocks
import alfheim.common.core.handler.AlfheimConfigHandler
import alfheim.common.entity.EntityElf
import alfheim.common.world.dim.alfheim.structure.StructureDreamsTree
import net.minecraft.entity.passive.*
import net.minecraft.init.Blocks
import net.minecraftforge.common.BiomeDictionary
import net.minecraftforge.common.BiomeDictionary.Type
import ru.vamig.worldengine.*

open class BiomeAlfheim @JvmOverloads constructor(r: Boolean = false): WE_Biome(WE_WorldProvider.we_id, r) {
	
	init {
		setBiomeName("Alfheim")
		
		BiomeDictionary.registerBiomeType(this, Type.MAGICAL)
		
		clearSpawn()
		setColor(0xA67C00)
		waterColorMultiplier = 0x1D1D4E
		
		createChunkGen_InXZ_List.clear()
		createChunkGen_InXYZ_List.clear()
		decorateChunkGen_List.clear()
		
		addEntry(EntityElf::class.java, AlfheimConfigHandler.elvesSpawn)
		addEntry(EntitySheep::class.java, AlfheimConfigHandler.sheepSpawn)
		addEntry(EntityPig::class.java, AlfheimConfigHandler.pigSpawn)
		addEntry(EntityChicken::class.java, AlfheimConfigHandler.chickSpawn)
		addEntry(EntityCow::class.java, AlfheimConfigHandler.cowSpawn)
	}
	
	fun addEntry(clazz: Class<*>, rate: IntArray) {
		val (w, i, x) = rate
		spawnableCreatureList.add(SpawnListEntry(clazz, w, i, x))
	}
	
	override fun getFloatTemperature(x: Int, y: Int, z: Int): Float {
		return if (AlfheimCore.winter) 0f else 0.5f
	}
	
	companion object {
		val dreamTree = StructureDreamsTree(AlfheimBlocks.altWood1, AlfheimBlocks.altLeaves, 3, 7, 11, 15)
		val sadOak = StructureDreamsTree(Blocks.log, Blocks.leaves, 0, 4, 8, 4)
	}
}